from userservice import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(255))
    lastname = db.Column(db.String(255))
    phonenumber = db.Column(db.BigInteger, unique=True, nullable=False)
    status = db.Column(db.String(40))

    def __init__(self, firstname, lastname, phonenumber, status):
        self.firstname = firstname
        self.lastname = lastname
        self.phonenumber = phonenumber
        self.status = status

    def __repr__(self):
        return '<User %d>' % self.id
