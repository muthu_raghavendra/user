import json
from flask import request, jsonify, Blueprint, abort
from flask.views import MethodView
from userservice import db, app
from userservice.product.models import User

Usermain = Blueprint('Usermain', __name__)


@Usermain.route('/')
@Usermain.route('/home')
def home():
    return "Welcome To JUMPPOPSHOP"


class UserView(MethodView):

    def get(self, id=None, page=1):
        if not id:
            users = User.query.paginate(page, 25).items
            res = {}
            for user in users:
                res[user.id] = {
                    'firstname': user.firstname,
                    'lastname': user.lastname,
                    'phonenumber': user.phonenumber,
                    'status': user.status
                }
        else:
            user = User.query.filter_by(id=id).first()
            if not user:
                abort(404)
            res = {
                'firstname': user.firstname,
                'lastname': user.lastname,
                'phonenumber': user.phonenumber,
                'status': user.status
            }
        return jsonify(res)

    def post(self):
        firstname = request.form.get('firstname')
        lastname = request.form.get('lastname')
        phonenumber = request.form.get('phonenumber')
        status = request.form.get('status')
        user = User(firstname, lastname, phonenumber, status)
        db.session.add(user)
        db.session.commit()
        return jsonify({user.id: {
            'firstname': user.firstname,
            'lastname': user.lastname,
            'phonenumber': str(user.phonenumber),
            'status': user.status
        }})


user_view = UserView.as_view('user_view')
app.add_url_rule(
    '/user/', view_func=user_view, methods=['GET']
)
app.add_url_rule(
    '/user/create', view_func=user_view, methods=['POST']
)
app.add_url_rule(
    '/user/<int:id>', view_func=user_view, methods=['GET']
)
